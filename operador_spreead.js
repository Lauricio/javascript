
// ele vai espalhar os elementos de um array
// tornando cada elemento individual 

const arr = [12,132,45,22,41,75,11]

console.log(arr)
//aqui sera retornado um array contendo os elementos por indice
console.log(...arr)
// aqui cada elemento sera retornado diretamente

// exemplo de uso 
// tenho uma função que soma 3 notas que são rescebidas por parametro 
// e tenho um array que contem tres notas 
//na forma normal eu teria que especificar cada elemento passando o indice como no exemplo a seguir;

soma( array[1],array[2],array[3])

// com o uso do spread isso torna-se dispensavel, sendo que o spread torna individual cada parametro do array passando a quantidade de arrays como quantidade de parametros
// exemplo completo com uso  do spread.



const arr = [23,34,14]

function soma(n1,n2,n3){
    return n1 + n2 + n3 
}

console.log(soma(...arr))



//o spread pode tambem resceber mais de um elemento como parametro retornando um novo elemento com  a //junção de todos os elementos passados 
  //ex:


const n1  = [ 1,2,3]
const n2 = [3,5,6]

const n3 = [...n1, ...n2]

//sera retornado em n3 a junção dos arrays passados como spread








